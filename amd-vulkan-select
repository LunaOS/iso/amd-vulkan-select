#!/usr/bin/env bash

AMDVLK="amdvlk"
AMDGPUPRO="amdpro"
MESAVLK="radv"
MESAACO="radv_aco"
MESAGPL="radv_gpl"
ICD_DIR="/usr/share/vulkan/icd.d"

unset RADV_PERFTEST
export DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1=1

show_help() {
  echo "Usage: $0 driver name command to run"
  echo "Select the Vulkan driver option and the command to run."
  echo "If no arguments are provided, a GUI will open for driver selection."
  echo
  echo "Driver Name"
  echo "  $AMDVLK - Open-source Vulkan driver from AMD: A driver developed by AMD."
  echo "  $AMDGPUPRO - Vulkan driver from AMD: A proprietary driver from AMD with additional features and support."
  echo "  $MESAVLK - Open-source Vulkan driver for AMD GPUs: Part of the Mesa project, providing open-source support."
  echo "  $MESAACO - RADV variant using the ACO compiler."
  echo "  $MESAGPL - RADV varian using the LLVM compiler from AMDGPU-PRO."
  exit 1
}

if [ $# -lt 2 ]; then
  if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    show_help
  elif [ $# -lt 1 ]; then
    echo "Error: Command not provided."
    show_help
    exit 1
  fi
  OPTION=$(zenity --title "Vulkan driver selection" --list --radiolist \
    --text "Select driver to use" \
    --column "" --column "Driver" --column "Description" \
    --width 400 --height 250 \
    FALSE "$AMDVLK" "Open-source Vulkan driver from AMD" \
    FALSE "$AMDGPUPRO" "Proprietary Vulkan driver from AMD" \
    FALSE "$MESAVLK" "Open-source Vulkan driver for AMD GPUs" \
    FALSE "$MESAACO" "RADV variant using the ACO compiler" \
    FALSE "$MESAGPL" "RADV varian using the LLVM compiler")
  COMMAND_TO_RUN="$1"
else
  OPTION="$1"
  COMMAND_TO_RUN="$2"
fi

case "$OPTION" in
  "$AMDVLK")
    export VK_ICD_FILENAMES="${ICD_DIR}/amd_icd32.json:${ICD_DIR}/amd_icd64.json"
    ;;
  "$AMDGPUPRO")
    export VK_ICD_FILENAMES="${ICD_DIR}/amd_pro_icd32.json:${ICD_DIR}/amd_pro_icd64.json"
    ;;
  "$MESAVLK")
    export VK_ICD_FILENAMES="${ICD_DIR}/radeon_icd.i686.json:${ICD_DIR}/radeon_icd.x86_64.json"
    ;;
  "$MESAACO")
    export VK_ICD_FILENAMES="${ICD_DIR}/radeon_icd.i686.json:${ICD_DIR}/radeon_icd.x86_64.json"
    export RADV_PERFTEST=aco
    ;;
  "$MESAGPL")
    export VK_ICD_FILENAMES="${ICD_DIR}/radeon_icd.i686.json:${ICD_DIR}/radeon_icd.x86_64.json"
    export RADV_PERFTEST=gpl
    ;;
  *)
    echo "Error: Invalid driver name $OPTION"
    show_help
    ;;
esac

if [ -n "$OPTION" ]; then
  if [ -n "$(command -v $COMMAND_TO_RUN)" ]; then
    echo "Running $COMMAND_TO_RUN with the selected Vulkan driver: $OPTION"
    echo "VK_ICD_FILENAMES: $VK_ICD_FILENAMES"
    if [ -n "$RADV_PERFTEST" ]; then
      echo "RADV_PERFTEST: $RADV_PERFTEST"
    fi
    eval "$COMMAND_TO_RUN"
    exit $?
  else
    echo "Command $COMMAND_TO_RUN not found"
  fi
else
  exit 1
fi
